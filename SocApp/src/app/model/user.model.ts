export class User {
    userId: number;
    name: string;
    isLocked: boolean;
    password: string;
    phoneNumber:string;
    username: string;
}

export class UserInRole{
    id: number;
    userId: number;
    roleId: number;
}
