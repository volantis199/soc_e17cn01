import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import {  FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  addRegisterFrm = new FormGroup({
    name: new FormControl('', [ Validators.required]),
    phoneNumber: new FormControl('', [Validators.required]),
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })
  
  constructor(
    private router: Router,
    public service: UserService, 
  ) { }

  ngOnInit() {
    this.createRegisterForm();
  }

  createRegisterForm() {
    this.addRegisterFrm
  }

  get registerFormControl() {
    return this.addRegisterFrm.controls;
  }

  onToLogin() {
    this.router.navigateByUrl("sign-in");
  }
  
  onRegister() {
    
  }

}
