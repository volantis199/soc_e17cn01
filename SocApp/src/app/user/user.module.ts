import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserModalItemComponent } from './user-modal-item/user-modal-item.component';
import {FormsModule} from '@angular/forms';
import { UserComponent } from './user.component';
import { RoleComponent } from '../role/role.component';


@NgModule({
  declarations: [
    UserModalItemComponent,
    FormsModule,
    RoleComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RoleComponent
  ],
  exports:[
    UserComponent
]
})
export class UserModule { }
