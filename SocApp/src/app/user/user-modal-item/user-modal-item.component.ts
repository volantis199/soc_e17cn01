import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms'
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-user-modal-item',
  templateUrl: './user-modal-item.component.html',
  styles: [
  ]
})
export class UserModalItemComponent implements OnInit {
  fullName: string;
  locked: any;
  phoneNumber: string;
  username: string;
  password: string;
  formGroup: FormGroup;
  isReadOnly: boolean = false;
  id: number;
  isAddMode: boolean;
  constructor(public usersrc: UserService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      phoneNumber: [null, Validators.compose([Validators.required])],
      username: [null, Validators.compose([Validators.required])],
      fullName: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      locked: [null, null]
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.formGroup && this.formGroup.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) &&
      (control.dirty || control.touched);
    return result;
  }

  submit(){
    this.isAddMode = !this.usersrc.formData.userId;
    if(this.isAddMode){
      this.usersrc.addUser().subscribe((response: any)=>{
        this.resetForm();
        this.usersrc.getAll();
        this.toastr.success("Add user successfully !", "User Information")
      },
      err => {
        console.log(err)
      });
    }
    else{
      this.usersrc.updateUser().subscribe((response: any)=>{
        this.resetForm();
        this.usersrc.getAll();
        this.toastr.info("Update user successfully !", "User Information")
      },
      err => {
        console.log(err)
      });
    }

  }

  resetForm(){
    this.formGroup.reset();
    this.usersrc.formData = new User();
  }

}
