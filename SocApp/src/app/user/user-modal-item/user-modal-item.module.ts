import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserModalItemComponent } from './user-modal-item.component';
import {FormsModule, FormBuilder, Validators} from '@angular/forms';


@NgModule({
  declarations: [UserModalItemComponent],
  imports: [
    CommonModule,
    FormsModule,
    FormBuilder,
    Validators
  ],
  exports:[
  ]
})
export class UserModalItemModule { }
