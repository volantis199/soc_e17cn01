import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RoleService } from '../services/role.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(public usersrc: UserService,
    private toastsr: ToastrService,
    public rolesrc: RoleService) { }

  isUserView: boolean = true;
  isRoleView: boolean = false;
  ngOnInit(): void {
    this.usersrc.getAll();
    this.rolesrc.getAll();
  }

  populateForm(item: any){
    this.usersrc.formData = Object.assign({}, item)
  }
  populateRole(item: any){
    this.rolesrc.roleData = Object.assign({}, item);
  }

  deleteUser(id: any){
    if(confirm("Do you want to delete this record?")){
      this.usersrc.delete(id).subscribe((res: any)=> {
      this.toastsr.error("Delete succesfully !", "User Information");
      this.usersrc.getAll();
    
    },
    err =>{
      this.toastsr.error("Error when delete!!!");
      console.log(err);
    })
  }}

  deleteRole(id: any){
    if(confirm("Do you want to delete this record?")){
      this.rolesrc.deleteRole(id).subscribe((res: any)=> {
      this.toastsr.error("Delete succesfully !", "Role Information");
      this.rolesrc.getAll();
    },
    err =>{
      this.toastsr.error("Error when delete!!!");
      console.log(err);
    })
  }}

  switchRoleView(){
    this.isRoleView = true;
    this.isUserView = false;
  }

  switchUserView(){
    this.isRoleView = false;
    this.isUserView = true;
  }
}
