import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { User } from '../model/user.model';
import {Role} from '../model/role.model';
import { UserService } from '../services/user.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  fullName: string;
  locked: any;
  phoneNumber: string;
  username: string;
  password: string;
  formGroup: FormGroup;
  isReadOnly: boolean = false;
  id: number;
  isAddMode: boolean;
  constructor(public rolesrc: RoleService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      roleName: [null, Validators.compose([Validators.required])],
      description: [null, null]
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.formGroup && this.formGroup.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) &&
      (control.dirty || control.touched);
    return result;
  }

  submit(){
    this.isAddMode = !this.rolesrc.roleData.roleId;
    if(this.isAddMode){
      this.rolesrc.addRole().subscribe((response: any)=>{
        this.resetForm();
        this.rolesrc.getAll();
        this.toastr.success("Add user successfully !", "Role Information")
      },
      err => {
        console.log(err)
      });
    }
    else{
      this.rolesrc.updateRole().subscribe((response: any)=>{
        this.resetForm();
        this.rolesrc.getAll();
        this.toastr.info("Update user successfully !", "Role Information")
      },
      err => {
        console.log(err)
      });
    }

  }

  resetForm(){
    this.formGroup.reset();
    this.rolesrc.roleData = new Role();
  }
}
