import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Role } from '../model/role.model';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) { }
  readonly apiUrl = environment.apiUrl+'/api/role';

  roleData: Role = new Role();
  listRole: Role[];
  getAll(){
    return this.http.get(this.apiUrl)
    .toPromise().then(res => this.listRole = res as Role[]);
  }

  addRole(){
    return this.http.post(this.apiUrl, this.roleData);
  }

  updateRole(){
    return this.http.put(this.apiUrl, this.roleData);
  }

  deleteRole(id: number){
    return this.http.delete(this.apiUrl +"/" + id);
  }
}
