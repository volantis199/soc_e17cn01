import { Injectable } from '@angular/core';
import { User } from '../model/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  readonly apiUrl = environment.apiUrl+'/api/user';

  formData: User = new User();
  listData: User[];

  addUser(){
    return this.http.post(this.apiUrl, this.formData);
  }
  
  getAll(){
    this.http.get(this.apiUrl).toPromise().then(x => this.listData = x as User[]);
  }

  updateUser(){
    return this.http.put(this.apiUrl + "/" + this.formData.userId, this.formData);
  }

  delete(id:number){
    return this.http.delete(this.apiUrl + "/" +id);
  } 
}
