﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsLocked { get; set; }

        public string Password { get; set; }
        public string PhoneNumber { get; set; }

        public string Username { get; set; }
    }

    public class UserInRoleModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }

}
