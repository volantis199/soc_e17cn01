﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class BIllModel
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public double TotalPrice { get; set; }
        public double Discount { get; set; }
        public int UserId { get; set; }
    }

    public class ProductInBillModel
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
        public int BillId { get; set; }
    }
}
