﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class ProductModel
    { 
        public int Id { get; set; }
        public string BrandName { get; set; }

        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Origin { get; set; }
        public string Name { get; set; }
        public double InPrice { get; set; }
        public double OutPrice { get; set; }
        public int CategoryId { get; set; }
    }

}
