﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebManagement_E17CN01
{
    [Table("Product")]
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        [Column(TypeName ="nvarchar(100)")]
        public string BrandName { get; set; }
        [Column(TypeName = "nvarchar(100)")]

        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Origin { get; set; }
        public string Name { get; set; }
        [Required]
        public double InPrice { get; set; }
        [Required]
        public double OutPrice { get; set; }
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }

        //collections
        public ICollection<ProductInBill> ProductInBills { get; set; }

    }

}
