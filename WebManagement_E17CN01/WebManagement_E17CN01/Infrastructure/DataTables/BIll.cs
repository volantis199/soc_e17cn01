﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebManagement_E17CN01
{
    [Table("Bill")]
    public class Bill
    {
        [Key]
        public int BillId { get; set; }
        public string Address { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public double TotalPrice { get; set; }
        public double Discount { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

        //collections
        public ICollection<ProductInBill> ProductInBills { get; set; }

    }

    [Table("ProductInBill")]
    public class ProductInBill
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
        public int BillId { get; set; }


        [ForeignKey("BillId")]
        public Bill Bill { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
    }
}
