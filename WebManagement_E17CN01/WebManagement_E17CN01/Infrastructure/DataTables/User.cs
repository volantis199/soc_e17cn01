﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebManagement_E17CN01
{
    [Table("User")]
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string Name { get; set; }
        public bool IsLocked { get; set; }
        [Column(TypeName = "nvarchar(MAX)")]
        [Required]
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string Username { get; set; }

        //collections
        public ICollection<UserInRole> UserInRoles { get; set; }
        public ICollection<Bill> Bills { get; set; }

    }

    [Table("UserInrole")]
    public class UserInRole
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }

        [ForeignKey("UserId")]

        public User User { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
    }

}
